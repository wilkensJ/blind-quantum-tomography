# -*- coding: utf-8- -*-
"""

This python document defines several functions to prepare, run and
store numerical simulations.
The function prearrangement() prepares the dictionaries and directory
for storing the files, then calls the function runRecoveryAlg() where the
random signal and random measurements are drawn with which the data vector
y is produced. The signal is an object of class csSignal.MulticsSignal and
the measurement is an object of the class Measurement.MultiMeasurement.
Then the function SDT() or ALS() is called to recover the signal from
the measurement matrix and the given data vector y.
The original signal and recovered signals along with some algorithm and other
recovery data can be stored in a directory in .h5py files.

The functions are used in the numerical study in

    Roth, Wilkens, Hangleiter and Eisert. 
    "Semi-device-dependent blind quantum tomography" 
    https://arxiv.org/abs/2006.03069

Authors: 
    Jadwiga Wilkens, Ingo Roth and Dominik Hangleiter


Licence: 
    This project is licensed under the MIT License -
    see the LICENSE.md file for details.
"""

import numpy as np
import random
import h5py
import Measurement
import csSignal
import os
import scipy
from scipy.linalg import *
import datetime
import scipy.sparse as scisp
import copy
from prettytable import PrettyTable

roundTo = 5
np.set_printoptions(precision = roundTo)

def prearrangement(sigMeasParDict={}, algParDict={}, boolDict={}, ALSDict={}):
    """
    This function, if storeFiles == True, finds a not yet existing directory
    to store all the data in, writes all dictionaries into the
    labBook.txt file to be able to recreate the numerics,
    stores the (same) parameters into generalParameter.h5
    which is in the same folder as the numerics data will be stored in.
    Runs the runRecoveryAlg() function int:runs times
    original and recovered signal will be stored in same directory
    as generalParameter.h5
    Input:
        dict:sigMeasParDict with
            "N":            int, denotes the number of blocks in signal and
                                 in measurement
            "dim"           int, denotes the dimension of one block in bigX
                                 and int:dim**2 gives the number of columns in
                                 each measurement in curlA
            "rank"          int, the rank of each block in bigX
            "sparsity"      int, how many blocks are not zero in bigX
            "listM"         list, the recovery algorithm tries to recover
                                  given data produced by measurement curlA
                                  where the number of rows denotes the amount
                                  of small single measurements in each block
                                  and is given by the numbers in list:listM.
                                  For each entry in list:listM a new
                                  measurement is drawn.
            "whichMType"    str, sets the type of measurement which is used
            "calibCoeffMultiply"    float, optional parameter to alter the
                                           normally drawn calibration
                                           coefficients by multiplying
            "calibCoeffOffset"      float, optional parameter, adds this
                                           number to (already multiplied)
                                           normally drawn random calibration
                                           coefficients
        dict:algParDict with
            "it"
            "thresh"
            "runs"
            "repetitions"
            "listTomographyTypes"      str, either "SDT", "demixingT" or
                                         "cheatDemixingT"
            "numberOfShots"         int, optional, if given, shot noise will
                                         be simulated
        dict:boolDict with
            "initializationHack"
            "calibration"
            "compareWStandardT"
            "storeFiles"
            "labBook"
            "printVariables"
            "randomTargetCoeff"
            "firstTraceOne"     boolean, optional, if True, during
                                         thresholding the support, first block
                                         will be trace normalized
            "allTraceOne"       boolean, optional, if True, during
                                         thresholding the support, all blocks
                                         will be trace normalized
        ALSDict={} with
            "alternatingIt"
            "initRandom"
    return:
        str:dirName, the directory name in which the numerical simulation data
                     is stored.
    """
    # To store results in h5py file, directory
    # general parameters and an entry in labBook.txt is created
    if checkIfExists(boolDict, "storeFiles"):
        # Create the numericalData directory
        if not os.path.exists("numericalData"):
            os.mkdir("numericalData")
        # Create a not yet existing directory to store the .h5 files in which
        # will be created during the computation of the recovery algorithm.
        # count will get bigger by one if the directory already exists.
        count = 0
        # Searches for directories, starts with
        # 1Simulation/ then 2Simulation/,...
        # until a not existing directory is found
        while os.path.exists("numericalData/%sSimulation/" % count):
            count += 1
        # Create the directory name
        dirName = "numericalData/" + str(count) + 'Simulation/'
        # Create directory itself
        os.mkdir(dirName)
        algParDict["dirName"] = dirName
        # Create the titles of all filenames,
        # every file deals with another random drawn signal.
        titles = np.array([dirName + sigMeasParDict["whichMType"] +
                           'N' + str(sigMeasParDict["N"]) +
                           'dim' +str(sigMeasParDict["dim"]) +
                           'rank' + str(sigMeasParDict["rank"]) +
                           'sparsity' + str(sigMeasParDict["sparsity"]) +
                            '-' + str(x) + '.h5'
                           for x in range(1, algParDict["runs"] + 1)])
        # To know immediately in which directory the data is stored
        print("Directory in which data is stored is: " + dirName)
        # Stores all dictionaries into generalParameters.txt in
        # the newly created directory where all other data is stored too
        writeParamsInFileInDir(sigMeasParDict, algParDict, boolDict, ALSDict)
        # Run the runRecoveryAlg for int:algParDict["runs"] time
    # Write all important information into labBook.txt file
    writeBasicLabBookGeneralParams(sigMeasParDict,
                                   algParDict,
                                   boolDict,
                                   ALSDict)
    for ii in range(algParDict["runs"]):
        if checkIfExists(boolDict, "storeFiles"):
            algParDict["fileName"] = titles[ii]
        # Make sure that no redundancy is falsify numerical simulation
        np.random.seed()
        runRecoveryAlg(sigMeasParDict,
                       algParDict,
                       boolDict,
                       ALSDict)
    return dirName

def writeBasicLabBookGeneralParams(sigMeasParDict,
                                   algParDict,
                                   boolDict,
                                   ALSDict):
    """
    Writes given dictionaries into labBook.txt line by line.
    The lines are  splitted with " : ".
    """
    # Create a dictionary to store time and a comment
    labBookDict = {}
    # First write the date, time and directory the data is stored in
    now = datetime.datetime.now()
    # To add something, for example which computer is used
    addThis = "nothing"
    # addThis = input("What I want to add to labBook.txt" +
                    # " for this numerical simulation run: ")
    labBookDict["time"] = now
    labBookDict["comment"] = addThis
    file = open("numericalData/labBook.txt", "a")
    file.write("\n\n +------+ New simulation +------+ \n")
    file.close()
    writeToTxt("numericalData/labBook.txt", labBookDict, sigMeasParDict, algParDict,
                                                    boolDict, ALSDict)

def writeParamsInFileInDir(sigMeasParDict, algParDict, boolDict, ALSDict):
    """
    Creates a txt file name in given directory
    and stores all dictionaries of this numerical simulation run
    into the file generalParameters.txt
    """
    fileName = algParDict["dirName"] + "/generalParameters.txt"
    writeToTxt(fileName, sigMeasParDict, algParDict, boolDict, ALSDict)


def writeToTxt(fileName, *args):
    """
    This function writes arbitrary amount of dictionaries into a .txt file
    Input:
        str:fileName  the directory in which the dictionaries are written
        *args          the dictionaries are stored here
    """
    file = open(fileName, "a")
    for dictionary in args:
        for key in dictionary:
            file.write(key + ' : ' +str(dictionary[key]) + "\n")
    file.close()


def runRecoveryAlg(sigMeasParDict, algParDict, boolDict, ALSDict):
    print('running recovery')
    """
    Creates N original signals which have to be recovered (bigX).
    Creates N measurements (curlA_i) with i=1,...,N
    which are together one big measurement(curlA).
    The amount of rows in the measurements can be adjusted and iterated over.
    Produces the data y = curlA(bigX).
    Call the desired recovery algorithm and "hand over the data and
    measurement along with all necessary" parameters.
    Saves original signals, recovered signals and
    the recovery parameters into h5py files.

    If list:algParDict["listTomographyTypes"] has more than one object in list,
    SDT runs for every item in list to compare these different statuses.

    If boolean:boolDict["compareWStandardT"] is True,
    SDT tries to recover only first block of bigX with first block of curlA
    (meaning the correction terms are not taken into account)
    with the data produces WITH corrections, meaning the whole curlA and bigX
    is used.

    Input:
        dict:sigMeasParDict 
        dict:algParDict
        dict:boolDict
        dict:ALSDict
           
    """
    # Initialize a dictionary for saving all arising parameters from the
    # recovery process
    saveRecovParams = {}
    # Extract the parameters used many time from the dictionary
    N, dim = sigMeasParDict["N"], sigMeasParDict["dim"]
    rank,sparsity = sigMeasParDict["rank"], sigMeasParDict["sparsity"]
    # Generate measurements according to the
    # variable str:sigMeasParDict["whichMType"]
    whichMType = sigMeasParDict["whichMType"]
    # The amount of rows (i.e. small measurements A)
    # in the big measurements changes every Loop
    for m in sigMeasParDict["listM"]:
        print('Number of measurements ', m)
        saveRecovParams["m"] = m
        # Every entry in curlA is drawn from i.i.d Gaussian distribution
        # And every row is normalized to l2 norm is one
        if whichMType == "MultiMeasurementGauss":
            multiMeasure = Measurement.MultiMeasurementGauss(N, m, dim * dim)
        # The small measurement A are all  randomly drawn Pauli words
        # "without putting back" 
        if whichMType == "MultiMeasurementPauli":
            multiMeasure = Measurement.MultiMeasurementPauli(N, m, dim * dim)
        # curlA_1 is the target measurement, or block
        # It is drawn like "MultiMeasurementPauli" measurement 
        # curlA_i for i = 2, ..., 7 are each correction measurements
        # Where a Pauli matrix X, Y, Z is flipped to another Pauli matrix
        # either X, Y or Z respectively,
        if whichMType == "CalibrationWithoutIdentity":
            multiMeasure = Measurement.CalibrationWithoutIdentity(N,
                                                                  m,
                                                                  dim * dim)
        # Take just curlA_5 of above described measurement
        # Mainly for analyzing the algorithm
        if whichMType == "FifthsBlockMMP":
            multiMeasure = Measurement.FifthsBlockMMP(1, m, dim * dim)
        # Just take N blocks out of curlA WITHOUT the target block
        if whichMType == "NCorrectionBlockDiscriminationMMP":
            multiMeasure = Measurement.NCorrectionBlockDiscriminationMMP(
                                                                    N,
                                                                    m,
                                                                    dim * dim)
        # Just take N-1 blocks plus the target block
        if whichMType == "NCalibrationSettingMMP":
            multiMeasure = Measurement.NCalibrationSettingMMP(N, m, dim * dim)
        # Same measurement scheme as "CalibrationWithoutIdentity"
        # Only difference is that the identity is mapped to X, Y, Z and
        # vice versa
        if whichMType == "CalibrationMMP":
            multiMeasure = Measurement.CalibrationMMP(N, m, dim * dim)
        # To save computing time and storage,
        # use one measurement for int:algParDict["repetitions"] times
        # and draw the original signal randomly:
        for jj in range(algParDict["repetitions"]):
            print('iteration',jj)
            # To avoid redundancy in pseudo random numbers
            np.random.seed()
            # Draw randomly support of signal bigX, 
            # the calibration parameters and the original array of signals
            originalSignal, support, calibCoeff = drawRandSignal(
                                                               sigMeasParDict,
                                                               boolDict)
            if checkIfExists(boolDict, "storeFiles"):
                saveRecovParams["currentRepetition"] = jj
                saveRecovParams["originalSignal"] = originalSignal
                saveRecovParams["support"]        = support
                saveRecovParams["calibCoeff"]     = calibCoeff
            # For checking if the algorithm is working or not and why
            if checkIfExists(boolDict, "printVariables"):
                print("+------+ new signal +------+\n\t")
                print("original frob norm block wise: \n\t",
                      originalSignal.fullFrobArray[::,None])
            # Data given to algorithm to restore signals, y=curlA(bigX)
            data = multiMeasure.dot(originalSignal)
            # For simulating quantum experiments, add shot noise
            if checkIfExists(algParDict, "numberOfShots"):
                # For every entry in data vector, add noise
                for kk in range(len(data)):
                    data[kk] = simulateShotNoise(data[kk],
                                                 algParDict["numberOfShots"])
            # list:listTomographyTypes can have up to 3 entries,
            # SDT is being run for the same random signal and
            # random measurement for every item in list:listTomographyTypes

            for tomographyType in algParDict["listTomographyTypes"]:
                print('Performing tomography of type', tomographyType)
                # If cheating is activated, the SDT works with a known support
                # and a new value "support" is added to dict:sigMeasParDict
                if tomographyType == "cheatDemixingT":
                    sigMeasParDict["support"] = support
                if tomographyType == "demixingT":
                    # All indices are assumed to be active
                    sigMeasParDict["support"] = np.arange(0,N)
                # If an ALSDict is given, not the 
                if tomographyType == "ALS":
                    # Don't use one time SDT, use many times!
                    # And if it doesn't work, try again with different random
                    # initialization if wanted
                    numberOfTries = ALSDict["numberOfTries"]
                    thresh        = algParDict["thresh"]
                    # Initialize paramters to control break up
                    count, objectiveFunc, objectiveFToCompare = 0, 1, 1
                    # Now do the ALS thing int:numberOfTries many times
                    # or less
                    while count < numberOfTries and objectiveFunc > thresh:
                        # Use the alternating least square algorithm
                        testRecSignal, objectiveFunc, neededIt = ALS(data,
                                                               multiMeasure,
                                                               sigMeasParDict,
                                                               algParDict,
                                                               ALSDict,
                                                               boolDict)
                        # Make sure that the best recovered signal is used
                        if objectiveFunc < objectiveFToCompare or count == 0:
                            recoveredSignal = testRecSignal
                            objectiveFToCompare = objectiveFunc + 0.
                        count += 1
                else:
                    # Start SDT algorithm, return the recovered signal object,
                    # the last objective function and the needed iterations
                    recoveredSignal, objectiveFunc, neededIt = SDT(data,
                                                        multiMeasure,
                                                        tomographyType,
                                                        sigMeasParDict,
                                                        algParDict,
                                                        boolDict)
                print('Success')
                # Calculate distance between original and recovered signals
                deviationValue = calcDeviationValue(originalSignal,
                                                    recoveredSignal)
                # For checking during simulation
                print(tomographyType + " deviationValue, " + str(m)
                       + " m, run " + str(jj) + ": ", deviationValue)
                # Save the basic parameters into dictionary saveRecovParams
                saveRecovParams["toIdentify"]     = tomographyType
                saveRecovParams["deviationValue"] = deviationValue
                # For printing and comparing the original and recovered
                # signals
                if checkIfExists(boolDict, "printVariables"):
                    pTable = PrettyTable(["original singV",
                                            "calib params",
                                            "recovered singV"])
                    pTable.add_row([originalSignal.fullFrobArray[::,None],
                        calibCoeff,
                        recoveredSignal.fullSingValues[::None]])
                    print(pTable)
                if checkIfExists(boolDict, "storeFiles"):
                    saveRecovParams["objectiveFunc"]  = objectiveFunc
                    saveRecovParams["neededIt"]       = neededIt
                    saveRecovParams["recoveredSignal"] = recoveredSignal
                    saveToFile(algParDict, saveRecovParams)
                else:
                    writeToTxt("labBook.txt", saveRecovParams)

            # The first block of the measurement from before is used for
            # trying to recover the data produced from above:
            if checkIfExists(boolDict, "compareWStandardT"):
                print('Now I am comparing with standard tomography.')
                # Only first block needed, store first block of measurement
                # drawn above into oneMeasure
                # For that first initiate an empty single measurement and
                # fill it with the first block of multiMeasure
                # Create empty MultiMeasurement 
                oneMeasure = Measurement.EmptyMultiMeasurement(1,
                                                               m,
                                                               dim * dim)
                # Assign the first block of the before used random measurement
                # to this one block
                if type(multiMeasure.fullMeasurement) == \
                                                scipy.sparse.coo.coo_matrix:
                    multiMeasureMatrix = multiMeasure.\
                                                    fullMeasurement.toarray()
                    firstBlock = multiMeasureMatrix.reshape(N,
                                                            m,
                                                            dim * dim)[0]
                else:
                    firstBlock =  multiMeasure.fullMeasurement[0]
                oneMeasure.fullMeasurement = np.array([firstBlock])
                # Perform the SDT with the one block measurement and same data
                recoveredTarget, objectiveFunc, neededIt = SDT(
                                                          data,
                                                          oneMeasure,
                                                          "compareWStandardT",
                                                          sigMeasParDict,
                                                          algParDict,
                                                          boolDict)
                # Calculate distance between original target block and
                # recovered target block
                # To use existing structures, create an empty signal and
                # assign the first block of the original signal to it
                emptySignal = csSignal.MultiMyMatrix(1, dim)
                emptySignal.multiMatrix = np.array(
                                              [originalSignal.multiMatrix[0]])
                # Calculate the deviation of the target blocks
                targetDeviationValue = calcDeviationValue(emptySignal,
                                                          recoveredTarget)
                # Save the basic parameters into dict:saveRecovParams
                saveRecovParams["toIdentify"]     = "compareWStandardT"
                saveRecovParams["deviationValue"] = targetDeviationValue
                if checkIfExists(boolDict, "storeFiles"):
                    saveRecovParams["objectiveFunc"]  = objectiveFunc
                    saveRecovParams["neededIt"]       = neededIt
                    saveRecovParams["recoveredSignal"] = recoveredTarget
                    saveToFile(algParDict, saveRecovParams)
                else:
                    writeToTxt("labBook.txt", saveRecovParams)
                # For checking during simulation
                print("standardT, " + str(m) + " m, run "
                                    + str(jj) + ": ", targetDeviationValue)

def SDT(data,
        multiMeasure,
        tomographyType,
        sigMeasParDict,
        algParDict,
        boolDict):
    """
    Recovers multi block signal from data (y) and measurements (multiMeasure)
    Input: 
            np.ndarray:data      of size (m,1) gets produced through
                                curlA(multiSignal)
            Measurement.MultiMeasurement:multiMeasure measurement object with
                                                      which the data was
                                                      produced
            str:tomographyType  denotes the knowledge regarding the sparsity
                                status the recovery algorithm can use to
                                recover the signal
            dict:sigMeasParDict 
            dict:algParDict  
            dict:boolDict
                            with parameter described in function
                                        prearrangement()
           

    SDT recovers int:N low rank signals with shape int:dim times int:dim from
    int:N times int:m small measurements;
    int:it limits the repetitions of recovery algorithm
    np.ndarray:support can be a vector with indices for the multi block signal 
    or which signals are zero or which measurements are not active
    """
    # Extract parameters important for SDT
    # For comparing the N and sparsity has to be set to 1
    if tomographyType == "compareWStandardT":
        N, sparsity = 1, 1
    else:
        N, sparsity = sigMeasParDict["N"], sigMeasParDict["sparsity"]
    dim, rank,  = sigMeasParDict["dim"], sigMeasParDict["rank"]
    # Because these variable appear more often than others
    initializationHack = boolDict["initializationHack"]
    firstTraceOne = checkIfExists(boolDict, "firstTraceOne")
    allTraceOne   = checkIfExists(boolDict, "allTraceOne")
    # Initialize int:count which remembers number of carried out iterations 
    # float:objectiveFunc gets initialized and set to one
    count, objectiveFunc = 0, 1.
    # Initialize an empty csSignal object where the recovery process is stored
    signalRecover = csSignal.MulticsSignal(N, dim, rank, sparsity)

    # For letting the target measurement recover faster
    if initializationHack and tomographyType != "compareWStandardT":
        # Let the algorithm believe there is only one signal to recover
        # After int:countOut the sparsity is set to original value again 
        signalRecover.sparsity = 1
        # Get only the target measurement
        targetMeasurementMatrix = multiMeasure.subMeasureMatrix(0)
        # Set the vector to zero
        signalRecover.multiVector = np.zeros((dim**2 * N))
        # Replace the first signal in vector storage with the inverse of
        # the target measurement times data
        signalRecover.multiVector[:dim**2] = np.linalg.pinv(
                                targetMeasurementMatrix.toarray()).dot(data)
    elif type(checkIfExists(sigMeasParDict, "givenInitialization")) == np.ndarray:
        # An initialization signal is given, for example in ALS recovery 
        signalRecover.multiVector = sigMeasParDict["givenInitialization"]
    else:
        # Compute the inverse of measurement acting on data
        signalRecover.multiVector = multiMeasure.dot(data, daggered=True)
    # int:countOut determines when the sparsity is
    # set to the original value again
    if initializationHack and checkIfExists(boolDict, "calibration"):
        countOut = 50
    else:
        countOut = 1
    # Threshold the rank of the computed signals if int:dim is one, the
    # function will not do anything
    signalRecover.HardThresholdingRank()
    # Calculate the support if required
    if tomographyType == "SDT":
        sigMeasParDict["support"] = signalRecover.calculateSupport()
    # If there is only one block in bigX, thresholding the support
    # makes no sense and the function is not doing anything
    # Here the block(s) can be trace normalized
    signalRecover.makeMultiSignalSparsed(
                               sigMeasParDict["support"],
                               firstTraceOne,
                               allTraceOne)
    # Loop at most int:algParDict["it"] times over the SDT algorithm
    # And break the loop if the objective function is smaller than a threshold
    while objectiveFunc > algParDict["thresh"] and count < algParDict["it"]:
        # rl=y-Sum(Ai(Xi)) (residual):
        rl = data - multiMeasure.dot(signalRecover)
        # Initialize gradient with empty object
        gradient = csSignal.MultiMyMatrix(N, dim)
        # Set the vector form
        gradient.multiVector = multiMeasure.dot(rl, daggered=True)
        # Projects gradient onto tangent space of multi signal
        projectedGradient = signalRecover.projectTangentspace(gradient)
        # Just in case the algorithm is running into completely wrong direction
        if np.linalg.norm(projectedGradient) > 1e10:
            print("out")
            break
        # Array of step widths computed with projected matrices
        # and multi measurement
        mu = stepwidth(projectedGradient, multiMeasure, dim, N)
        # Compute new multi signal with gradient step and step width
        signalRecover.multiMatrix = signalRecover.multiMatrix +\
                                                        mu * projectedGradient
        # Threshold rank of signals
        signalRecover.HardThresholdingRank()
        # Set the intern variable signalRecover.sparsity to int:sparsity again
        # if criteria are met
        if initializationHack and count == countOut:
            signalRecover.sparsity = sparsity
        # Calculate the support if tomographyType is set to SDT
        if tomographyType == "SDT":
            sigMeasParDict["support"] = signalRecover.calculateSupport()
        # Threshold support
        signalRecover.makeMultiSignalSparsed(
                               sigMeasParDict["support"],
                               firstTraceOne,
                               allTraceOne)
        # Compute the current objective function
        objectiveFunc = calcBreakCriteria(multiMeasure, signalRecover, data)
        # Just for checking if algorithm is working or not
        if checkIfExists(boolDict, "printVariables"):
            pTable = PrettyTable(["it", "mu", "blocknorms pr.G",
                              "signal blocknorms", "objectiveF"])
            pTable.add_row([count, np.array([mu]).reshape(N,1),
                np.linalg.norm(projectedGradient, axis=(1, 2))[::,None],
                signalRecover.fullFrobArray[::,None],
                round(objectiveFunc, roundTo)])
            print(pTable)
        # int:count for aborting in case the SDT needs to many iterations
        count = count + 1
    # Return the recovered signal, the last objective function and
    # the number of needed iterations
    return signalRecover, objectiveFunc, count


def checkIfExists(gDict, gKey):
    """
    Tests if the given dictionary dict:gDict has a key str:gKey.
    If so, the value is returned, if not the returned value is False.
    Input:
        dict:gDict
        str:gKey
    Output:
        value of gDict[gKey] or False
    """
    if gKey in gDict:
        value = gDict[gKey]
    else:
        value = False
    return value

def calcBreakCriteria(gMultiMeasure, gSignals, gData):
    """
    Input: 
            Measurement.MultiMeasurement:multiMeasure 
            cSignal.MulticsSignal:signals     
            np.ndarray:data 
    Return:
            float:objectiveFunc distance of given data vector and data vector
                               calculated with recovered multi signal
    """
    testData = gMultiMeasure.dot(gSignals)
    objectiveFunc = np.linalg.norm(testData - gData) / np.linalg.norm(gData)
    return objectiveFunc

def calcDeviationValue(gOriginalSignal, gRecoveredSignal):
    """
    Calculate the distance or deviation in l2 norm of the two
    # given signal objects
    Input:
        csSignal.MulticsSignal:gOriginalSignal
        csSignal.MulticsSignal:gRecoveredSignal
    Output:
        float:deviationValue
    """
    # Compute the vector deviation of the given signals
    residue = gRecoveredSignal.multiVector - gOriginalSignal.multiVector
    # Calculate the l2 norm of the deviation and normalize by l2 norm
    # of csSignal.MulticsSignal:originalSignal
    dev = np.linalg.norm(residue) / np.linalg.norm(gOriginalSignal.multiVector)
    return dev

def drawRandSignal(sigMeasParDict, boolDict):
    """
    This function generates a random bigX signal with its support and
    calibration coefficients.
    Input:
        dict:sigMeasParDict with
            "N"             int, the amount of blocks
            "dim"           int, the dimension of each matrix X
            "sparsity"      int, how many blocks are active
            "rank"          int, the rank of the matrix X
        boolDict with
            "calibration"       boolean, if True first block always active and
                                         all other indices drawn randomly
                                         if False all entries randomly drawn
            "randomTargetCoeff" boolean, if True, first block calibration
                                        coefficient is random too    
    Output:
        np.ndarray:support                      active indices of block signal
        csSignal.MulticsSignal:signal  block signal
    """
    # Extract the needed parameters 
    N, dim = sigMeasParDict["N"], sigMeasParDict["dim"] 
    sparsity, rank = sigMeasParDict["sparsity"], sigMeasParDict["rank"]
    # If calibration is active, the first block is always active
    if checkIfExists(boolDict, "calibration"):
        # For the calibration setting, the first measurement is the
        # target measurement therefore always active
        targetIndex = np.array([0])
        # These are all support indices of the correction blocks
        correctionBlocks = list(np.arange(1, N))
        # Draw the support of the sparse correction block entries
        correctionBlocksActive = random.sample(correctionBlocks, sparsity - 1)
        # Sort the indices for a better overview
        correctionBlocksActiveSorted = np.sort(correctionBlocksActive)
        # Merge the target block with the
        # randomly drawn correction block indices
        support = np.append(targetIndex, correctionBlocksActiveSorted)
    else:
        # If calibration is False, all support indices are drawn randomly
        support = np.sort(random.sample(list(np.arange(0, N)), sparsity))
    # Now create the randomly drawn csSignal object
    # with given parameters and (randomly drawn) indices.
    # All blocks are set to the same signal
    # which consists out of a Haar-random unitary matrix U and
    # int:r many singular value
    signal = csSignal.MulticsSignal(N, dim, rank, sparsity, init_Random=True)
    # The block signal is now fully filled and
    # has to be made sparse with the randomly drawn support
    signal.makeMultiSignalSparsed(support)
    # For the calibration setting the calibration coefficients are needed
    if boolDict["calibration"]:
        # Draw random calibration parameters and weight original array
        # (only the active blocks) of signals with these random parameters
        randCalibCoeff = drawRandCalibCoeff(sigMeasParDict,
                                            boolDict["randomTargetCoeff"])
        # Multiply the signal by calibration parameters
        signal.fullSingValues[support] = signal.fullSingValues[
                                         support] * randCalibCoeff[::, None]
    else:
        # For simplicity set all calibration coefficients to one
        randCalibCoeff = np.ones(sparsity)
    # Return the drawn np.ndarray:support and the signal
    return signal, support, randCalibCoeff


def drawRandCalibCoeff(sigMeasParDict, randomTargetCoeff):
    """
    Draw normal calibration coefficients multiplied by int:multiplyBy
    and added with int:offset.
    Input:
        dict:sigMeasParDict needs at least
            "sparsity"             int, denotes the amount of drawn
                                        coefficients
        boolean:randomTargetCoeff       denotes of the first entry is
                                        random too or not
    Output
        np.ndarray:randCalibCoeff   array of length int:sparsity
    """
    # Extract int:sparsity
    sparsity = sigMeasParDict["sparsity"]
    # First check if the optional parameters are given
    if "calibCoeffMultiply" in sigMeasParDict:
         multiplyBy = sigMeasParDict["calibCoeffMultiply"]
    else:
         multiplyBy = 1.
    if "calibCoeffOffset" in sigMeasParDict:
        offset = sigMeasParDict["calibCoeffOffset"]
    else:
        offset = 0.
    # Draw random int:sparsity many numbers as calibration coefficients
    randCalibCoeff = np.random.normal(0, 1, sparsity) * multiplyBy + offset
    # The calibration setting requires the first entry to be 1.
    # But it can be turned of if boolean:randomTargetCoeff == True
    if not randomTargetCoeff:
        randCalibCoeff[0] = 1.
    return randCalibCoeff


def saveToFile(algParDict, saveRecovParams):
    """
    Saves all relevant parameters to assess how SDT performed dependent on
    certain parameters into a h5py file with data saved in different branches
    of file
    Input:
        dict:algParDict
        dict:saveRecovParams      
    """
    # Extract all necessary parameters and objects out of dictionaries
    fileName = algParDict["fileName"]
    m        = saveRecovParams["m"]
    jj       = saveRecovParams["currentRepetition"]
    originalSignal  = saveRecovParams["originalSignal"]
    deviationValue  = saveRecovParams["deviationValue"]
    objectiveFunc   = saveRecovParams["objectiveFunc"]
    neededIt        = saveRecovParams["neededIt"]
    tomographyType  = saveRecovParams["toIdentify"]
    recoveredSignal = saveRecovParams["recoveredSignal"]
    support         = saveRecovParams["support"]
    calibCoeff      = saveRecovParams["calibCoeff"]
    # Open the h5py file in read and edit mode
    filehf = h5py.File(fileName, "a")
    # Create a group for the original signals for the amount of
    # used measurements m
    # Only if this group does not exist jet
    checkIfGroupExists = str(m) + "originalMultiSignals" + str(jj) in  filehf
    if not checkIfGroupExists:
        grp_UsedSignals = filehf.create_group(
            str(m) + "originalMultiSignals" + str(jj))
        # Store the singular value decomposition matrices for less storage space
        grp_UsedSignals.create_dataset(
            "fullSingValues", data=originalSignal.fullSingValues)
        grp_UsedSignals.create_dataset("fullU", data=originalSignal.fullU)
        # The signals could be not hermitian, than fullU != fullV
        if not originalSignal.hermitian:
            grp_UsedSignals.create_dataset("fullV", data=originalSignal.fullV)
        # To make the data analyzes easier, store the randomly drawn support
        # and calibration coefficients
        grp_UsedSignals.create_dataset("IndicesOfActiveUsers", data=support)
        grp_UsedSignals.create_dataset("CalibrationParameters", data=calibCoeff)
    # Now create the group for the recovered signals
    # Or open it if it already exists
    checkIfGroupExists = str(str(m) + "amountOfMeasures") in  filehf
    if checkIfGroupExists:
        grp = filehf[str(m) + "amountOfMeasures"]
    else:
        grp = filehf.create_group(str(m) + "amountOfMeasures")
    # Since one original signal can be used for different measurements
    # the repetition jj is important
    # And the tomographyType because the DT, informedDT and SDT can be
    # compared
    grp.create_dataset(str(jj) +
                    "neededIterations" + str(tomographyType), data=neededIt)
    grp.create_dataset(str(jj) +
                    "objectiveFunc" + str(tomographyType), data=objectiveFunc)
    grp.create_dataset(str(jj) + 'deviationValue' +
                           str(tomographyType), data=deviationValue)
    # Check if the recoverdSignal object is a MulticsSignal with the SVD
    # decomposition
    if type(recoveredSignal) == csSignal.MulticsSignal:
        grpRecSignal = grp.create_group(str(jj) + "recoveredMultiSignals" +\
                                                        str(tomographyType))
        grpRecSignal.create_dataset("fullSingValues" + str(tomographyType),
                                        data=recoveredSignal.fullSingValues)
        grpRecSignal.create_dataset("fullU" + str(tomographyType),
                                                data=recoveredSignal.fullU)
        if not recoveredSignal.hermitian:
            grpRecSignal.create_dataset("fullV" + str(tomographyType),
                                                data=recoveredSignal.fullV)
    # The recovered Signal could be a csSignal.MultiMyMatrix too
    elif type(recoveredSignal) == csSignal.MultiMyMatrix:
        grpRecSignal = grp.create_group(str(jj) + "recoveredMultiSignals" +\
                                                        str(tomographyType))
        grpRecSignal.create_dataset("multiMatrix" + str(tomographyType),
                                        data=recoveredSignal.multiMatrix)
    # Close the file, all important parameters are stored
    filehf.close()


def simulateShotNoise(x, numberOfShots):
    """
    Draws a random sample from a (complex in comments) binomial distribution
    with expectation of x.
    Input:
        float:x
        int:numberOfShots
    Output:
        float:The altered value x
    """
    # Since this function simulates quantum experimental data, the value x
    # must lie between 1 and -1
    if np.abs(x) > 1:
        raise ValueError("Expectation value must be between -1 and 1.")

    pReal = .5 * (x.real + 1)
    # pImag = .5 * (x.imag + 1)

    realPart = (2 * np.random.binomial(numberOfShots, pReal, 1) /
                                                numberOfShots - 1)

    # imagPart = (2 * np.random.binomial(numberOfShots, pImag, 1) /
    #             numberOfShots - 1)

    return realPart[0] # + 1j * imagPart[0]


def stepwidth(inputArray, multiMeasure, dim, N):
    """
    Calculates the needed step width recovery algorithm for every block
    in one shot.
    If the inputArray consists of only one matrix or only one vector,
    one value is calculated and returned
    Step width defined as
    mu = ( ||multIMatrix||_F**2 / ||multiMeasure(inputArray)||_2**2 )^2
    where ||...||_F denotes the Frobenius norm
    Input: 
            np.ndarray:inputArray of size(N,d,d), the multi matrix of the
                                   signal with which the step width should be
                                   calculated
            Measurement.MultiMeasurement:multiMeasure   
    Return:
            np.ndarray:stepwidths of size (N, 1 ,1), the block wise step
                                  widths for every signal element written in
                                  one row vector
            or
            float:stepwidth       one stepwise for the whole signal

    """
    if N == 1 or dim == 1:
        # If inputArray is a column vector, it has to be flattened
        if inputArray.shape[1]==1 and inputArray.shape[2]==1:
            inputArray = inputArray.flatten().reshape(-1,1)
        # Compute multiMeasure(inputArray)
        denominator = multiMeasure.dot(inputArray)
        # Compute norm of multiMeasure(inputArray)
        # In case it gets zero, take the value 1e-12 to note divide by zero
        normDenominator = np.max([np.linalg.norm(denominator),1e-12])
        # Compute ||inputArray||_F, if its zero take 1e-12
        normNominator   = np.max([np.linalg.norm(inputArray),1e-12])
        # Calculate actual step width
        stepwidth = normNominator**2 / normDenominator**2
        return stepwidth
    else:
        # Compute norm of every single matrix in inputArray and make column
        # vector out of row vector
        m = multiMeasure.m
        normCounter = np.linalg.norm(inputArray, axis=(2, 1))[::, None]
        # Compute multiMeasure(inputArray)
        denominator = multiMeasure.blockDot(inputArray)
        denominator = denominator.reshape(N, m)
        # Compute norm of every single matrix in
        # block_matrix(curlA)* inputArray
        normDenominator = np.linalg.norm(denominator, axis=1)
        # To not divide by zero, set them to one
        zeroIndices = np.where(normDenominator == 0)[0]
        normDenominator[zeroIndices] = 1
        # Reshape it into a column vector
        normDenominator = normDenominator[::, None]
        # Calculate actual step width
        stepwidths = normCounter**2 / normDenominator**2
        # Reshape it into a column vector and return
        return stepwidths.reshape(N, 1, 1)


def ALS(yData,
        multiMeasurement,
        sigMeasParDict,
        algParDict,
        ALSDict,
        boolDict):
    """
    Alternates between holding the coefficients and the
    density matrix constant.
    """
    # Extract all important parameters
    dim      = sigMeasParDict["dim"]
    N        = sigMeasParDict["N"]
    rank     = sigMeasParDict["rank"]
    sparsity = sigMeasParDict["sparsity"]
    it, thresh = algParDict["it"], algParDict["thresh"]
    alternatingIt = checkIfExists(ALSDict, "alternatingIt")
    initRandom    = checkIfExists(ALSDict, "initRandom")
    printVariables = checkIfExists(boolDict, "printVariables")
    # Initialize the crieterias to break the iteration loop
    altCount, breakCriteria = 0., 1.
    # Initialize MultyMyMatrix objects
    optimizeXi = csSignal.MultiMyMatrix(N,1,rank=rank,sparsity=sparsity)
    optimizeRho = csSignal.MulticsSignal(1,dim,rank,sparsity)
    # Draw starting signals random if wanted
    if initRandom:
        np.random.seed()
        # MultiscSignal has an init random function!
        randomSignal = csSignal.MulticsSignal(1,
                                              dim,
                                              rank,
                                              sparsity,
                                              init_Random=True)
        # Take the first signal
        rho = randomSignal.multiMatrix[0]
    else:
        # Just take the unit matrix
        rho = np.eye(dim)
    if initRandom:
        # Initialize xi filled with zeros
        xi = np.zeros(N)
        # Draw random support
        sup = np.sort(random.sample(list(np.arange(1, N)), sparsity))
        # Draw random calibration parameters
        calibParams = np.random.normal(0, 1, sparsity)
        # Set the support to random indices
        xi[sup] = calibParams
        # Reshape the array blockwise
        xi.reshape(N,1)
    else:
        # Just take ones
        xi = np.ones(N)


    # Go through the alternating process several times
    while altCount < alternatingIt and breakCriteria > thresh:
        # Set the optimize objects with updated rho and xi
        optimizeRho.multiMatrix = rho.reshape(1,dim,dim)
        optimizeXi.multiMatrix = xi.reshape(N,1,1)
        # Construct curlA_rho, rho is fixed
        measurement_rho = multiMeasurement.evaluateWithGivenArray(
                                                              "densityMatrix",
                                                              rho)
        sigMeasParDict["givenInitialization"] = optimizeXi.multiVector
        # Since the dimension of the single block are one, dim has to be
        # set to one in the dictionary for the SDT algorithm
        sigMeasParDict["dim"] = 1
        # Optimize xi
        xiObject, _, _ = SDT(yData,
                             measurement_rho,
                             "SDT",
                             sigMeasParDict,
                             algParDict,
                             boolDict)
        # Extract the multiMatrix for the evaluateWithGivenArray function
        xi = xiObject.multiMatrix
        # And here set back
        sigMeasParDict["dim"] = dim
        # With optimized xi construct curlA_xi
        measurement_xi = multiMeasurement.evaluateWithGivenArray(
                                                    "calibrationCoefficients",
                                                    xi.flatten())
        sigMeasParDict["givenInitialization"] = optimizeRho.multiVector
        # Since the amount of blocks is one, N has to be set to one in the
        # dictionary for the SDT algorithm
        sigMeasParDict["N"] = 1
        # Optimize rho
        rhoObject, _, _ = SDT(yData,
                              measurement_xi,
                              "",
                              sigMeasParDict,
                              algParDict,
                              boolDict)
        # Extract the multiMatrix for the evaluateWithGivenArray function
        rho = rhoObject.multiMatrix
        # And here set back
        sigMeasParDict["N"] = N
        # Normalize rho
        rho = rho/np.linalg.norm(rho.reshape(dim,dim),"nuc")
        # Check if already recovered
        breakCriteria = calcBreakCriteria(measurement_xi, rho, yData)
        print('The cost function in iteration {} is'.format(altCount),breakCriteria )
        altCount += 1
        # Repeat
    # Empty the sigMeasParDict["givenInitialization"]
    sigMeasParDict["givenInitialization"] = False
    # Put the recovered array and matrix in one csSignal object
    RmultiMatrix = csSignal.MulticsSignal(N, dim, rank, sparsity)
    RmultiMatrix.multiMatrix = np.kron(xi, rho)
    return RmultiMatrix, breakCriteria, altCount