# -*- coding: utf-8- -*-
"""

The function storeRelevantParametersInTxtFile() processes the data from a
certain directory where the simulation data from the SDTandALS.py funcitons
are stored in.
The processed data is stored in a .txt file and can be further used to plot
relevant recovery parameters.

The function is used in the numerical study data plotting in

    Roth, Wilkens, Hangleiter and Eisert. 
    "Semi-device-dependent blind quantum tomography" 
    https://arxiv.org/abs/2006.03069

Authors: 
    Jadwiga Wilkens, Ingo Roth and Dominik Hangleiter


Licence: 
    This project is licensed under the MIT License -
    see the LICENSE.md file for details.
"""

import numpy as np
import h5py
import ast

def storeRelevantParametersInTxtFile(dirName, nameForStorage):
    # Extract the general parameters
    # First get the filename
    generalParametersFile = dirName + "generalParameters.txt"
    # Define the dictionary in which the general parameters are then stored
    fileParamsDict = {}
    # Store all titles
    saveTitleList = []
    # Open the generalParameters file and split the lines
    # To store every line in the dicionary
    # Like "N" : "4\n" -> filesParamDict["N"] = "4"
    with open(generalParametersFile) as f:
        for line in f:
           (key, val) = line.split(" : ")
           fileParamsDict[key] = val[:-1]
    # To make the rest easier
    runs     = int(fileParamsDict['runs'])
    maxIter  = int(fileParamsDict['it']) 
    threshForAlg   = fileParamsDict['thresh']  
    # listM must be a real list, not a string of a list
    listM    = fileParamsDict['listM']
    # So convert it into a real list
    listM    = ast.literal_eval(listM)
    typeM    = fileParamsDict['whichMType']
    N        = int(fileParamsDict['N'])           
    dim      = int(fileParamsDict['dim'])
    sparsity = int(fileParamsDict['sparsity']) 
    rank     = int(fileParamsDict['rank']) 
    repetitions   = int(fileParamsDict['repetitions']) 
    # Same for listM
    listTomographyTypes = fileParamsDict['listTomographyTypes']
    listTomographyTypes = ast.literal_eval(listTomographyTypes)
    standardT = fileParamsDict['compareWStandardT']
    # Set the threshold for signals being recovered to this
    thresh = 1e-03
    # Add the tomography type "compareWStandardT" if required
    if standardT == "True":
        listTomographyTypes.append('compareWStandardT')
    # Iterate through all the tomography types
    # And store the data in seperate files
    for tomographyType in listTomographyTypes:
        # First build the titles which are all in the directory
        title = np.array([dirName + str(typeM) + 'N' + str(N) +
                          'dim' + str(dim) +'rank' + str(rank) +
                          'sparsity' + str(sparsity)  + '-' + str(x) + '.h5'
                          for x in range(1, int(runs) + 1)])
        # Then build the lists filled with zeros 
        # So the zero can be replaced with data values
        RecOrNotRec = [np.zeros(len(listM))]
        devValuesBig   = [np.zeros(len(listM))]
        neededItsBig  = [np.zeros(len(listM))]
        objectiveFunctionsBig  = [np.zeros(len(listM))]
        traceNormsBig = [np.zeros(len(listM))]
        calibrationcoefficientNormBig  = [np.zeros(len(listM))]
        # Now iterate over all the titles
        for name in title:
            # Open the file
            f = h5py.File(name, 'r')
            # Iterate over the repetitions where the signal stays the same
            for ii in range(int(repetitions)):
                # Initiate the empty lists
                deviationValues = []
                neededIterations = []
                objectiveFunctions = []
                traceNorms = []
                calibCoeffDeviation = []
                # Last but not least iterate over the different amount of
                # measurements used to produce the data and recover the signal
                for k in listM:
                    # Extract the active indides
                    activeIndices = f[str(k) + "originalMultiSignals" + str(ii) +
                                      "/IndicesOfActiveUsers"]
                    # Open the group in which the specific recovered signal 
                    # is stored
                    grp = f[str(k) + "amountOfMeasures"]

                    # Extract the deviation value from original signal
                    deviationV = grp[str(ii) + 'deviationValue' +
                                         str(tomographyType)][()]
                    # Append the value to the list
                    deviationValues.append(deviationV)
                    # Append the beeded iterations to the list
                    neededIterations.append(grp[str(ii) + "neededIterations" +
                                            str(tomographyType)][()])
                    # And the objective function
                    objectiveFunctions.append(grp[str(ii) + "objectiveFunc" +
                                            str(tomographyType)][()])
                    # To calculate the trace norm, use the block with the biggest
                    # calibration coefficients of the original signal
                    # For that first get the original signal and the
                    # calibration coefficients of it
                    calibCoeffOriginal = f[str(k) + "originalMultiSignals" +
                                           str(ii) + "/CalibrationParameters"]
                    # Make it an array
                    calibCoeffOriginal = np.array(calibCoeffOriginal)
                    # Find the biggest entries index
                    biggestCalibCIndex = np.argmax(calibCoeffOriginal)
                    # Extract the active indices to get the biggest block
                    activeIndOriginal = f[str(k) + "originalMultiSignals" +
                                           str(ii) + "/IndicesOfActiveUsers"]
                    # Make it an array
                    activeIndOriginal = np.array(activeIndOriginal)
                    # Get the index of yhe biggest block!
                    biggestBlockIndex = activeIndOriginal[biggestCalibCIndex]
                    # Now extract the original signal
                    fullSingValuesOriginal = f[str(k) + "originalMultiSignals" +
                                               str(ii) + "/fullSingValues"]
                    # Make it an array and only get the important singular values
                    # until the index int:rank
                    biggestBlockSingVOriginal = np.array(fullSingValuesOriginal)\
                                        [biggestBlockIndex][:rank].reshape(rank,1)
                    # Extract the unitaries of the signals
                    fullUOriginal = f[str(k) + "originalMultiSignals" + str(ii) +
                                      "/fullU"]
                    # Make it an array and only get the important block
                    biggestBlockUOriginal = np.array(
                                        fullUOriginal)[biggestBlockIndex]
                    # Calculate Vdagg of singular value decomposition
                    biggestBlockVdaggOriginal = biggestBlockUOriginal.conj().T
                    # Compute the original signal block
                    matrixOriginal = np.dot(biggestBlockUOriginal[:,:rank],
                                     np.dot(biggestBlockSingVOriginal,
                                            biggestBlockVdaggOriginal[:rank]))
                    # Now compute the recovered signal block
                    # If the tomography Type is the standard tomography, there
                    # is only one block
                    if tomographyType == "compareWStandardT":
                        biggestBlockIndex = 0
                    fullSingValuesRec = grp[str(ii) +
                                          "recoveredMultiSignals" +
                                         str(tomographyType) +
                                         "/fullSingValues" + str(tomographyType)]
                    # Make it an array and only get the important singular values
                    # until the index int:rank
                    biggestBlockSingVRec = np.array(fullSingValuesRec)\
                                        [biggestBlockIndex][:rank].reshape(rank,1)
                    # Extract the unitaries of the signals
                    fullURec = grp[str(ii) + "recoveredMultiSignals"  +
                                      str(tomographyType) + "/fullU" +
                                      str(tomographyType)]
                    # Make it an array and only get the important block
                    biggestBlockURec = np.array(fullURec)[biggestBlockIndex]
                    # Calculate Vdagg of singular value decomposition
                    biggestBlockVdaggRec = biggestBlockURec.conj().T
                    # Compute the original signal block
                    matrixRec = np.dot(biggestBlockURec[:,:rank],
                                     np.dot(biggestBlockSingVRec,
                                            biggestBlockVdaggRec[:rank]))
                    # Normalize the recovered Matrix if it is not zero
                    if np.trace(matrixRec) != 0:
                        matrixRec /= np.trace(matrixRec)
                    # Compute the trace norm of the deviation of recovered matrix
                    # and original matrix
                    trNorm = np.linalg.norm(matrixRec.reshape(dim,dim) -
                                            matrixOriginal.reshape(dim,dim),
                                            ord = "nuc")
                    # Append this value to the trace norm list
                    traceNorms.append(trNorm)
                    # Now compute the difference of calibration coefficients
                    calibCoeffOriginal = np.sum(fullSingValuesOriginal, axis=1)
                    calibCoeffRecovery = np.sum(fullSingValuesRec, axis=1)
                    calibCoeffDeviation.append(np.linalg.norm(calibCoeffOriginal -
                                                              calibCoeffRecovery))
                # Make everything an array
                deviationValues     = np.array(deviationValues)
                neededIterations    = np.array(neededIterations)
                objectiveFunctions  = np.array(objectiveFunctions)
                traceNorms          = np.array(traceNorms)
                calibCoeffDeviation = np.array(calibCoeffDeviation)
                # And Append the just converted arrays to the big lists
                # Under each other
                devValuesBig = np.append(devValuesBig, [deviationValues], axis=0)
                neededItsBig = np.append(neededItsBig,
                                                [neededIterations],
                                                axis=0)
                objectiveFunctionsBig = np.append(objectiveFunctionsBig,
                                                  [objectiveFunctions], axis=0)
                traceNormsBig = np.append(traceNormsBig, [traceNorms], axis=0)
                calibrationcoefficientNormBig = np.append(
                                                    calibrationcoefficientNormBig,
                                                    [calibCoeffDeviation], axis=0)
            # Work with the file is done! Close it.
            f.close()
        # Repeat the listM for every value list
        bigListM = np.tile(listM,5)
        # Create the array which will be put into the .txt file
        # All the lists of calculated parameters and the listsMs 
        totalDataToSave = np.concatenate((devValuesBig,
                                          neededItsBig,
                                          objectiveFunctionsBig,
                                          traceNormsBig,
                                          calibrationcoefficientNormBig), axis=1)
        totalDataToSave = np.append([bigListM],totalDataToSave,axis=0)
        # Create the header
        header = "Total runs:" + str(runs*repetitions) + ", data from file " + \
                 dirName + ", typeM:" + typeM + \
                 " \n list m \n recoverd or not recoverd \n frob norm \n \
                 needed iterations \n breakUp criterias \n trace norm \n \
                 calibration coefficient deviation"
        # Final step in this iteration: store it in a file which is denoted with
        # the tomography type!
        saveTitle = nameForStorage  +str(tomographyType) + ".txt"
        print("Saving data here: "+  saveTitle)
        np.savetxt(saveTitle, totalDataToSave, header=header)
        saveTitleList.append(saveTitle)
    return saveTitleList