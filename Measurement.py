# -*- coding: utf-8- -*-
"""

This class produces measurement objects with which the recovery algorithms in SDTandALS.py
are run. It provides several methods for example for producing the data with a given
signal. There are many child classes with different type of drawing the
measurement matrix.

The class is used in the numerical study in

    Roth, Wilkens, Hangleiter and Eisert. 
    "Semi-device-dependent blind quantum tomography" 
    https://arxiv.org/abs/2006.03069

Authors: 
    Jadwiga Wilkens, Ingo Roth and Dominik Hangleiter


Licence: 
    This project is licensed under the MIT License -
    see the LICENSE.md file for details.
"""


import numpy as np
import scipy.sparse as scisp
from scipy.linalg import *
import csSignal
import pdb


class MultiMeasurement():
    """
    creates objects filled with information about measurements
    (Pauli or Gauss measurements are subclasses)
    
    Attributes:
        int:m,      amount of "small" measurements taken
                    in one big measurement matrix
        int:dd,     dimension of one big measurement matrix
        int:amount, gives number of single measurements in list
    Methods:

        multiMatrixFunction(dagg=False)
        constructFullMeasurement(m, dd)
        constructStringMeasureSDT(m, dd, amount)
        constructStringMeasureCalibration(m, dd)
        dot(givenThing,dagger=False)
        blockMatrix(dagg=False)
        blockDot(input,dagg==False) 
        rescaleBlockwise(gscale)
        coherenceBlockwise()
        fixDensityMatrix(gVector)
        fixCalibrationCoefficients(gCoefficients)
        fixVariable(nameOfConstant, gArray)
        coherenceBlockwise()
    
    Properties:
        multiMatrix
        multiMatrixDagg
    """

    def __init__(self, amount, m, dd):
        self.m = m
        self.dd = dd
        self.amount = amount

    def multiMatrixFunction(self, dagg=False):
        return np.array([])

    @property
    def multiMatrixDagg(self):
        return self.multiMatrixFunction(dagg=True)

    @property
    def multiMatrix(self):
        return self.multiMatrixFunction(dagg=False)

    def constructFullMeasurement(self, m, dd, amount):
        return np.array([])

    def constructStringMeasureSDT(self, m, dd, amount):
        pass

    def constructStringMeasureCalibration(self, m, dd):
        pass

    def dot(self, input, daggered=False):
        """
        Input: input [array(d1,d2) or csSignal] (matrix, vector or object)
               daggered [boolean] (optional, if daggered measurement matrix
               is needed)
        if type of input is csSignal, givenArray is set to the vectorForm of
        the csSignal
        if type of input is a matrix, the matrix gets vectorized to a column
        vector

        if dagger = False: np.dot(measurement,givenArray) 
        if dagger = True:  np.dot(measurement^dagger,givenArray) 

        return [array(m,1)]
        """

        # Initialize array so later it can be checked if with input can be
        # worked with:
        givenArray = np.array([])
        if daggered:
            # Take the daggered version of property
            multiMatrix = self.multiMatrixDagg
            # Reshape for making dot product easier
            multiMeasurement = multiMatrix.reshape(self.amount * self.dd,
                                                   self.m)
        else:
            # Stick single big not daggered A's together
            # to [A1,A2,...,Aamount]
            multiMeasurement = np.concatenate(self.multiMatrix, axis=1)
        # Check input
        # For multicsSignal and if the whole signal must be multiplied:
        if type(input) == csSignal.MulticsSignal or \
                                        type(input) == csSignal.MultiMyMatrix:
            # Easier to work with vectorised matrix
            givenArray = input.multiVector
        # For a numpy array:
        if type(input) == np.ndarray:
            # For not column vector:
            if input.shape[1] != 1:
                # Put all columns to one column together
                givenArray = np.array(input.T.reshape(input.size, -1))
            # For column vector:
            if input.shape[1] == 1:
                givenArray = np.copy(input)
                if len(input.shape)== 3 and input.shape[2] == 1:
                    givenArray = givenArray.flatten().reshape(-1,1)
        #If input was csSignal or array, givenArray is not empty:
        if givenArray.size:
            # Remember, if dagg=True, daggered version of matrixForm is used:
            try:
                product = np.dot(multiMeasurement, givenArray)
            except np.linalg.LinAlgError:
                raise Exception('wrong dimension of input')
        else:
            print(input)
            print('something went wrong, give me a csSignal or \
                                       an array! Set product to empty array.')
            # Set product to empty array
            product = np.array([])
        return product

    def blockMatrix(self, dagg=False):
        """
        [[A1],[A2], ... ,[AN]], blockwise written onto diagonal:
        [[A1]  [0]   [0]  ...  [0]
         [0]   [A2]  [0]  ...  [0]
          .           .         .
          .                .    .
          .                     .
         [0]        ...        [AN]]
        if dagg==True, the blockwise matrices are daggered
        """
        if dagg:
            multiMatrix = self.multiMatrixDagg
        else:
            multiMatrix = self.multiMatrix
        blockMatrix = block_diag(*multiMatrix)
        return blockMatrix

    def blockDot(self, input, daggered=False):
        """
        Input: input[array(amount,1)] column vector
        calcuates every Measurement times this given column vector
        [[A1]  [0]   [0]  ...  [0]    [[gVector],
         [0]   [A2]  [0]  ...  [0]     [gVector],
          .           .         .          .
          .                .    .          .
          .                     .          .
         [0]        ...        [AN]]   [gVector]]
        if daggered==True, the blockwise matrices are daggered
        Output: array [(amount*dim*dim,1)] column vector
        ???not [(m*amount,1)]???
        """
        # If it has already good shape to multiply it with blockMatrix:
        if input.shape[0] == self.dd * self.amount:
            bigVector = np.copy(input)
        # The input could come in block matrix form
        # Then the signal matrices have to be transposed and vectorized
        elif input.shape[0] == self.amount:
            # Transpose each block
            bigVector = np.transpose(input, (0, 2, 1))
            # Vectorize each block
            bigVector = bigVector.reshape(self.amount * bigVector.shape[1]**2,
                                                                           -1)
        else:
            print("Dimension wrong, vector of shape (" + str(self.m) +
                                                                ",1) needed!")
            print("dot product set to empty array.")
            return np.array([])
        # There are many zeros, sparse storage
        # and usage are not that costly
        blockMatrixM = scisp.csr_matrix(self.blockMatrix(dagg=daggered))
        try:
            product = blockMatrixM.dot(bigVector)
        except ValueError:
            bigVector = bigVector.flatten().reshape(-1,1)
            product = blockMatrixM.dot(bigVector)
        return product

    def rescaleBlockwise(self, gscales):
        """
        input: gscales type:np.ndarray or list
        this function changes the attribute self.fullMeasurement by rescaling
        the self.amount many blocks
        by the given scaling factors gscales
        """
        # Put array on a diagonal of a matrix
        diagMatrix = np.diag(gscales)
        # Check if already np.ndarray
        if type(self.fullMeasurement) == np.ndarray:
            # Take product block wise with np.einsum
            densefullMeasurement = np.einsum("ijk,il->ljk",
                                    self.fullMeasurement.reshape(self.amount,
                                                                 self.m,
                                                                 self.dd),
                                    diagMatrix)
        # If sparse matrix or list or ...
        else:
            # Take product block wise with np.einsum
            densefullMeasurement = np.einsum("ijk,il->ljk",
                        self.fullMeasurement.toarray().reshape(self.amount,
                                                               self.m,
                                                               self.dd),
                        diagMatrix)
        # Replace attribute self.fullMeasurement with the calculated
        # new measurement matrix
        self.fullMeasurement= scisp.csr_matrix(
                              densefullMeasurement.reshape(self.amount*self.m,
                                                           self.dd))

    def coherenceBlockwise(self):
        """
        returns a self.amount x self.amount matrix with blockwise coherence
        measures C_ij = ||A_i^dag * A_j||_inf (operator norm)
        """
        # First reshape the full measurement matrix
        # For already numpy vectors:
        if type(self.fullMeasurement) == np.ndarray:
            # Make to blockwise version
            A = self.fullMeasurement.reshape(self.amount,self.m,self.dd)
        # For no numpy vectors, like sparse version
        else:
            # Same as above but with .toarray()
            A = self.fullMeasurement.toarray().reshape(self.amount,
                                                       self.m,
                                                       self.dd)
        # Calculate all possible combinations of
        # all big measurement marices
        productInnerMatrices = np.einsum("mji,njl->mnil",A.conj(),A)
        # Take the frobenius norm of all the products
        coherenceMatrix      = np.linalg.norm(productInnerMatrices,
                                              ord="fro",
                                              axis=(2,3))
        return coherenceMatrix

    def fixDensityMatrix(self, gVector, gmeasMatr):
        """
        Return newMultiMatrix where the product of the
        np.ndarray:gVector and the single curlA measurement matrices
        are stored in columns of a new multi matrix
        input:
            np.ndarray:gVector
        return:
            np.ndarray:newMultiMatrix
        """
        # Calculate new measurement as 
        # (curlA_rho)_i = curlA_i(rho), i denotes the column of the 
        # curlA_rho matrix
        # Each column is the product of one measurement matrix with rho
        if len(gVector.shape) == 3:
            gVector = gVector[0]
        newMultiMatrix = np.einsum("ijk,k->ij",
                                    gmeasMatr, 
                                    gVector.T.flatten())
        return newMultiMatrix

    def fixCalibrationCoefficients(self, gCoefficients, gmeasMatr):
        """
        Return newMultiMatrix where the np.ndarray:gcoefficients
        are summed up together with the
        single curlA measurement matrices.
        input:
            np.ndarray:gcoefficients
        return:
            np.ndarray:newMultiMatrix
        """
        newMultiMatrix = np.einsum("ijk,i -> jk",gmeasMatr, gCoefficients)
        return newMultiMatrix


    def fixVariable(self, nameOfConstant, gArray, gmeasMatr):
        """
        This function sets the attribute self.fullMeasurement new
        the norm has to be adjusted
        input:
            str:nameOfConstant, "densityMatrix" or "calibrationCoefficients"
            np.ndarray:gArray, 
        """
        if type(self.fullMeasurement) != np.ndarray:
            notAnArray = True
        else:
            notAnArray=False
        if nameOfConstant == "densityMatrix":
            self.fullMeasurement = \
                        self.fixDensityMatrix(gArray, 
                                              gmeasMatr).reshape(1,
                                                                self.m,
                                                                self.amount)
            if notAnArray:
                self.fullMeasurement = \
                    scisp.csr_matrix(self.fullMeasurement.reshape(self.m,
                                                              self.amount))   
            self.dd = self.amount
            self.amount = 1
        elif nameOfConstant == "calibrationCoefficients":
            self.fullMeasurement = \
                self.fixCalibrationCoefficients(gArray,
                                                gmeasMatr).reshape(1,
                                                                   self.m,
                                                                   self.dd)
            if notAnArray:
                self.fullMeasurement = \
                    scisp.csr_matrix(self.fullMeasurement.reshape(self.m,
                                                                  self.dd)) 
            self.amount = 1
        else:
            print("wrong given string, either 'densityMatrix' or \
                                                   'calibrationCoefficients'")

    def evaluateWithGivenArray(self, nameOfConstant, gArray):
        """
        This function sets the attribute self.fullMeasurement new
        the norm has to be adjusted
        input:
            str:nameOfConstant, "densityMatrix" or "calibrationCoefficients"
            np.ndarray:gArray, 
        """
        measurementMatrix = self.multiMatrix
        if nameOfConstant == "densityMatrix":
            evaluatedMeasurementMatrix = \
                self.fixDensityMatrix(gArray, 
                                      measurementMatrix).reshape(self.amount,
                                                                 self.m,1)
            # Initialize an empty measurement
            # to fill it with the fixed measurement array
            newMeasuremementObject = EmptyMultiMeasurement(self.amount,
                                                           self.m, 
                                                           1)
        elif nameOfConstant == "calibrationCoefficients":
            evaluatedMeasurementMatrix = \
                self.fixCalibrationCoefficients(gArray,
                                                measurementMatrix).reshape(
                                                                      1,
                                                                      self.m,
                                                                      self.dd)
            # Initialize an empty measurement
            # to fill it with the fixed measurement array
            newMeasuremementObject = EmptyMultiMeasurement(1, self.m, self.dd)
        else:
            print("wrong given string, either 'densityMatrix' or \
                                                   'calibrationCoefficients'")
        # Set fullMeasurement atribute
        # to fixed measurement array
        newMeasuremementObject.fullMeasurement = evaluatedMeasurementMatrix
        return newMeasuremementObject






class MultiMeasurementPauli(MultiMeasurement):
    """
    creates an array filled with information about Pauli measurements

    Attributes:
        int:m 
            amount of measurements taken in one big measurement matrix
        int:dd
            dimension of big measurement
        int:amount          
            gives number of single measurements in list
        sparsearray:fullMeasurement 
            sparse matrix form of full measurement
        list:stringMeasurement
            stores information about all measurements in list filled with:
            strings = Pauli words
        dict: pauliMatrix
            a dictionary with pauli matrices "X","Y","Z","I"

    Methods:
        constructStringMeasureSDT(self,m,dd,amount) 
        constructStringMeasureCalibration(self,m,dd)
        constructStringsFromIndices(indices)
        constructFullMeasurement(m,dd)
        constructFullMultiMatrixForm(self,listOfStrings=None,dagg=False)
        pauliStringToMatrix(self,pauliString)
        multiMatrixFunction(dagg=False)
        subMeasureMatrix(self,k)
        howManyZeroRowsFifth(self)
        setupPauliDictionary(self) 
        blockMatrix(dagg=False)
        dot(givenThing,dagger=False)
        blockDot(input,dagg==False)
    """

    def __init__(self, amount, m, dd):
        self.setupPauliDictionary()
        self.m      = m
        self.dd     = dd
        self.amount = amount

        self.setupPauliDictionary()
        self.stringMeasurement = self.constructStringMeasureSDT(m,
                                                                dd, 
                                                                self.amount)
        self.fullMeasurement   = self.constructFullMeasurement(m,
                                                               dd, 
                                                               self.amount)

    def constructStringMeasureSDT(self, m, dd, amount):
        """
        return [array(amount,m),str] array with pauli word strings
        each row (or single measurements) stored in one list
        each big measurement (or curlA) stored in one list
        like [[['YI'], ['ZY']], [['XX'],['ZX']]]
        There are four pauli matrices: I,X,Y,Z
        for n qubit system p_i o p_i o ... p_i there are 4^n=dd possible
        different pauli words ordered with certain indices
        here indices are chosen as follows:
            if m*amount is smaller or equal than all possible pauli words,
            the pauli words are chosen randomly without repeating
            if m*amount is bigger, first they are chosen randomly without
            putting back (dd times),
                if the rest is still bigger than dd again they are drawn 
                randomly without putting back
                this will be repeated until the rest is smaller than dd,
                the rest is drawn again randomly without putting back
            means the amount each pauli word is drawn can be different only
            by one
        example:
            amount=5, m=10, dd= 8
            50//8= 6
            all pauli words are in the list of indices for at leas 6 time
            randomly spread
            then 50%8=2 2 pauli words are again drawn randomly without
            putting them back
        the method self.constructStringsFromIndices is used
        """
        # Indices of all possible pauli words = dd
        indices = np.arange(0, dd, dtype=int) 
        halfShuffled = np.array([])
        # Quotient without remainder, how many times dd fits in the amount of
        # needed pauli words,
        # This many times it has do be drawn pauli words from the whole set
        # without putting back
        for i in range(m * amount // dd):
            # append them without shuffeling them  
            halfShuffled = np.append(halfShuffled,
                                     np.random.permutation(indices))
        # Now append the rest without shuffeling
        halfShuffled = np.append(halfShuffled,
                                 np.random.choice(indices,
                                                  m * amount % dd,
                                                  replace=False))  
        # Now shuffle! like there where drawn randomly
        shuffledIndices = np.random.permutation(halfShuffled)
        # Make strings out of indices
        arrayPauliWords = self.constructStringsFromIndices(shuffledIndices)
        # Reshape into [array(amount,m)]
        finalArray = arrayPauliWords.reshape(amount, m, 1)
        return finalArray

    def constructStringMeasureCalibration(self, m, dd):
        """
        return [list] of the measurement strings
        constructs full measurements based on one given measurement
        in the end there are different variations of given measurement
        where in each variation one particular string (f.e. X) is replace by
        another particular string(f.e. Y)
            Iterate through the whole array entries
                Iterate through string of single measurements 
                replace in each iteration one particular single string with
                another particular string
                if there are more than one, replace them one after the other
                and add all possibilities together

        """
        # Needs a target measurement stored as strings
        # Has to be one list filed with Pauli words
        # like ['XY', 'IZ', ...]
        mainM = self.constructStringMeasureSDT(m, dd, 1)[0].flatten()
        # initiate list where final measurement is stored
        calibratedM = []
        # Use dictionary to work with numbers instead of
        # Pauli strings
        dic = {"I": 0, "X": 1, "Y": 2, "Z": 3}
        # For each entry 
        for entry in mainM:
            calibratedEntry = []
            for kk in range(4 * 4):
                calibratedEntry.append([])
            calibratedEntry[0].append(entry)

            for k in range(len(entry)):
                for l in range(1, 4):
                    currSymbIdx = dic[entry[k]]
                    replSymbIdx = (dic[entry[k]] + l) % 4
                    replSymb = list(dic.keys())[replSymbIdx]
                    calibratedEntry[currSymbIdx * 4 + replSymbIdx].append(
                                        entry[:k] + replSymb + entry[k + 1:])

            for kk in range(3, 0, -1):
                del calibratedEntry[kk * 4 + kk]

            calibratedM.append(calibratedEntry)

        calibratedMReversed = [list(x) for x in zip(*calibratedM)]
        return calibratedMReversed

    def constructStringMeasureCalibrationWithoutIdentityMapping(self,
                                                                m, 
                                                                dd, 
                                                                gtarget=False):
        """
        return [list] of the measurement strings
        constructs full measurements based on one given measurement
        in the end there are different variations of given measurement
        where in each variation one particular string (f.e. X) is replace by
        another particular string(f.e. Y)
            Iterate through the whole array entries
                Iterate through string of single measurements 
                replace in each iteration one particular single string with
                another particular string
                if there are more than one, replace them one after the other
                and add all possibilities together

        """
        if gtarget:
            mainM = gtarget
        else:
            mainM = self.constructStringMeasureSDT(m, dd, 1)[0].flatten()
        calibratedM = []
        dic = {"X": 0, "Y": 1, "Z": 2}

        for entry in mainM:
            calibratedEntry = []
            for kk in range(3 * 3):
                calibratedEntry.append([])
            calibratedEntry[0].append(entry)

            for k in range(len(entry)):
                for l in range(1, 3):
                    if entry[k] != "I":
                        currSymbIdx = dic[entry[k]]
                        replSymbIdx = (dic[entry[k]] + l) % 3
                        replSymb = list(dic.keys())[replSymbIdx]
                        calibratedEntry[currSymbIdx * 3 + replSymbIdx].append(
                            entry[:k] + replSymb + entry[k + 1:])
            for kk in range(2, 0, -1):
                del calibratedEntry[kk * 3 + kk]


            calibratedM.append(calibratedEntry)

        calibratedMReversed = [list(x) for x in zip(*calibratedM)]
        return calibratedMReversed

    def constructStringsFromIndices(self, indices):
        """
        Constructs a single measurement curlAsingle with self.m Pauli words
        in it denoted by strings.
        Given indices in [array] self.indicesPauliWords indicates the order
        of Pauli words.
        The vectorized pauli word matrices are written in the rows of one
        single measurement matrix with dimension (m,dd).

        first the tensor product of all Pauli words is calculated and put in
        tensorCharArrayIXYZ

        """
        d = int(np.sqrt(self.dd))
        charArrayIXYZ = np.char.array(list('IXYZ'))
        tensorCharArrayIXYZ = np.char.array(
            list('IXYZ'))  # char array ['I','X','Y','Z']
        # If dd is bigger than 4, the tensor product is calculated for each
        # dimension dd=(2^dimension)**2
        for i in range(int(np.log2(d)) - 1):
            # Gives matrix, must be flattened
            tensorCharArrayIXYZ = tensorCharArrayIXYZ[:, None] + charArrayIXYZ
            tensorCharArrayIXYZ = tensorCharArrayIXYZ.flatten()
        # if dd=8 tensorCharArrayIXYZ looks like:
        # ['III','IIX',...,'XXY',...,'ZZZ']
        arrayPauliWords = np.array([])
        for index in indices:  # constructs final array
            arrayPauliWords = np.append(
                arrayPauliWords, tensorCharArrayIXYZ[int(index)])
        return arrayPauliWords

    def constructFullMeasurement(self, m, dd, amount):
        """
        return sparse measurement matrix in shape (amount*m,dd)
        (to store it in sparse format!)
        """
        sparseMatrixcsr = self.constructFullMultiMatrixForm(dagg=False)
        # denseMatrixReshaped = denseMatrix.reshape(amount*m,dd)
        # sparseMatrixcsr = scisp.csr_matrix(denseMatrixReshaped)
        return sparseMatrixcsr

    def constructFullMultiMatrixForm(self, listOfStrings=None, dagg=False):
        """
        returns [scisp.csr_matrix(amount,m,dd)]
        multiMatrix [[A1], [A2], ... , [Aamount]]

        with
        A1 = [[vec(pauliWord1)],[vec(pauliWord2)], ..., [vec(pauliWordm)]]
        if dagg=True, all A's are daggered but still written in one row.
        Information about measurement is stored in strings
        (self.fullMeasurement) which denote the Pauli word.
        Strings need to be converted into matrices with
        self.pauliStringToMatrix(string) for ever Pauli word.
        """
        # Initializing array for usage for np.append
        if listOfStrings is None:
            listOfStrings = self.stringMeasurement
        amountM = len(listOfStrings)
        dim = int(np.sqrt(self.dd))
        for k in range(amountM):  # loop over all big measurements
            # for every loop emptying marray
            marray = scisp.csr_matrix((0, self.dd))
            # loop over all lists in the big measurement list
            for ll in listOfStrings[k]:
                # if there are more than one measurement or no measurements
                # in list, this is useful
                addMatrix = scisp.csr_matrix((dim, dim))
                # for every string in list:
                for string in ll:  
                    # convert one string into matrix
                    pauliWordMatrix = self.pauliStringToMatrix(string)  
                    # if there are more than one string, they are added
                    addMatrix = addMatrix + pauliWordMatrix
                # append next to existing array (shape = (something,) that is
                # a row array and will be a row array)
                marray = scisp.vstack(
                    [marray, addMatrix.reshape((1, self.dd))])
            # reshape it to m measurement with length dd
            marray = marray.reshape((self.m, self.dd))
            # Only the big measurement get daggered,
            # not the entire multiMatr
            if dagg == True:
                marray = marray.T.conj()
            if k == 0:
                finalArray = marray
            else:
                finalArray = scisp.vstack([finalArray, marray])

        return finalArray

    def pauliStringToMatrix(self, pauliString):
        """
        str:input is one Pauli word like 'XIZ'
        scisp.csr_matrix:return the equivalent tensor
        product matrix XoIoZ (o=tensor product).
        Iterate over the string and calculate the tensor product (kron)
        from right to left.
        """
        # Initialize for calculating tensor product
        matrixRep = scisp.csr_matrix([[1]])
        # For every character in Pauli word
        # calculate the tensor product
        for pauliChar in pauliString:
            # scisp.kron is here used as tensor product
            matrixRep = scisp.kron(matrixRep, self.pauliMatrix[pauliChar])
        return matrixRep

    def multiMatrixFunction(self, dagg=False):
        """
        return [array(amount,m,dd)] measurement matrix in 3
        dimensional matrix shape.
        First converts self.fullMeasurement into dense shape with shape
        (self.amount*self.m, self.dd).
        Then reshapes into (self.amount, self.m, self.dd)
        Calculates the norm of every row from each block
        And normalizes every row of each block with l2 norm
        """
        # First fullMeasurement is in sparse format
        sparseMatrixcsr = self.fullMeasurement
        # It has to be transformed to dense format
        denseMatrixReshaped = sparseMatrixcsr.toarray()
        # if rho is not fixed reshaped to
        # (self.amount, self.m, self.dd) dimensional array
        finalArray = denseMatrixReshaped.reshape(self.amount, self.m, self.dd)
        # To calculate the norm of every row vector
        normArray = np.sum(np.abs(finalArray)**2, axis=1)**(0.5)
        # Reshape to norm vector from row vector to column vector
        normArray = normArray.reshape(self.amount, 1, self.dd)
        # Set all zero entries to one
        normArray += np.array(normArray == 0, dtype=int)
        # Divide every row of matrix by the l2 norm of itself
        finalArray /= normArray
        if not dagg:
            return finalArray
        else:
            # transposes the single matrices inside the big matrix
            finalArray = np.transpose(finalArray, [0, 2, 1])
            finalArray = finalArray.conj()
            return finalArray

    def subMeasureMatrix(self, k):
        """
        return the kth measurement matrix in array form
        """
        return self.constructFullMultiMatrixForm(
                                  listOfStrings = [self.stringMeasurement[k]])

    def howManyZeroRowsFifth(self):
        """
        Just to check how many zero rows there are
        in the fifth measurement
        return amount of zeros rows
        """
        list2 = [x for x in self.stringMeasurement[5] if x == []]
        return len(list2)

    def setupPauliDictionary(self):
        """
        For converting Pauli words in string format
        into sparse scisp.csr_matrix matrix representation
        """
        X = 1 / np.sqrt(2) * np.array([[0, 1], [1, 0]])
        X = scisp.csr_matrix(X)
        Y = 1 / np.sqrt(2) * np.array([[0, -1j], [1j, 0]])
        Y = scisp.csr_matrix(Y)
        Z = 1 / np.sqrt(2) * np.array([[1, 0], [0, -1]])
        Z = scisp.csr_matrix(Z)
        self.pauliMatrix = {
            "I": scisp.csr_matrix(1 / np.sqrt(2) * np.eye(2)),
            "X": X,
            "Y": Y,
            "Z": Z
        }



class CalibrationMMP(MultiMeasurementPauli):
    """
    There is one target measurement and 12 correction terms
    Takes into account the 12 mappings:
    X->1, X->Y, X->Z, Y->1, Y->X, Y->Z, Z->1, Z->X, Z->Y,
    1->X, 1->Y, 1->Z
    """

    def __init__(self, amount, m, dd):
        self.m = m
        self.dd = dd
        self.amount = 13

        self.setupPauliDictionary()
        self.stringMeasurement = self.constructStringMeasureCalibration(
            m, dd)
        self.fullMeasurement = self.constructFullMeasurement(
            m, dd, self.amount)


class FifthsBlockMMP(MultiMeasurementPauli):
    """
    Just the fifth block of CalibrationMMP
    """

    def __init__(self, amount, m, dd):
        self.m = m
        self.dd = dd
        self.amount = amount

        self.setupPauliDictionary()
        self.stringMeasurement = \
                self.constructStringMeasureCalibrationWithoutIdentityMapping(
                                                                        m,
                                                                        dd,
                                                                        False)
        self.stringMeasurement = [self.stringMeasurement[5]]
        self.fullMeasurement = self.constructFullMeasurement(
            m, dd, self.amount)


class NCorrectionBlockDiscriminationMMP(MultiMeasurementPauli):
    """
    no target measurement
    only N (or self.amount) corrections of CalibrationMMP 
    """

    def __init__(self, amount, m, dd):
        self.m = m
        self.dd = dd
        self.amount = amount

        self.setupPauliDictionary()
        self.stringMeasurement = \
                self.constructStringMeasureCalibrationWithoutIdentityMapping(
                                                                           m,
                                                                           dd)
        self.stringMeasurement = self.stringMeasurement[1:amount + 1]
        self.fullMeasurement = self.constructFullMeasurement(
            m, dd, self.amount)


class NCalibrationSettingMMP(MultiMeasurementPauli):
    """
    one target measurement
    only N (or self.amount) corrections of CalibrationMMP 
    """

    def __init__(self, amount, m, dd):
        self.m = m
        self.dd = dd
        self.amount = amount

        self.setupPauliDictionary()
        self.stringMeasurement = \
                self.constructStringMeasureCalibrationWithoutIdentityMapping(
                                                                           m, 
                                                                           dd)
        self.stringMeasurement = self.stringMeasurement[0:amount]
        self.fullMeasurement = self.constructFullMeasurement(
            m, dd, self.amount)


class CalibrationWithoutIdentity(MultiMeasurementPauli):
    """
    Child class of MultiMeasurementPauli
    Does not take into account the mapping of X,Y,Z -> 1 and vice versa
    """

    def __init__(self, amount, m, dd, gtarget=False):
        self.m = m
        self.dd = dd
        self.amount = 7

        self.setupPauliDictionary()
        self.stringMeasurement = \
                self.constructStringMeasureCalibrationWithoutIdentityMapping(
                                                                      m,
                                                                      dd, 
                                                                      gtarget)
        self.fullMeasurement = self.constructFullMeasurement(
            m, dd, self.amount)
        

class EmptyMultiMeasurement(MultiMeasurementPauli):
    """
    Child class of MultiMeasurementPauli
    is first empty and can be filled
    only self.fullMeasurement stores the whole measurement
    and must be filled with an np.ndarray 
    """

    def __init__(self, amount, m, dd, gtarget=False):
        self.m = m
        self.dd = dd
        self.amount = amount
        self.fullMeasurement = np.array([])

    def multiMatrixFunction(self, dagg=False):
        """
        returns the desired dense np.ndarray:self.fullMeasurement
        if dagg=True, the single block matrices are transposed and conjugated
        """
        wholeDenseMeasurement = self.fullMeasurement
        if dagg:
            wholeDenseMeasurementTransp = np.transpose(wholeDenseMeasurement,
                                                       [0, 2, 1])
            wholeDenseMeasurementDagg = wholeDenseMeasurementTransp.conj()
            return wholeDenseMeasurementDagg
        else:
            return wholeDenseMeasurement


class MultiMeasurementGauss(MultiMeasurement):
    """
    MultiMeasurement class with Gaussian Measurements

    Attributes:
        int:,       number of measurements (or rows) of every A
        int:dd,     length of measurements (row)
        int:amount, number of A's
                    corresponds to number of blocks in signal
        np.ndarray:fullMeasurement

    Methods:
        constructFullList(m,dd)
        multiMatrixFunction(dagg=False)
    """

    def __init__(self, amount, m, dd):
        MultiMeasurement.__init__(self, amount, m, dd)
        self.fullMeasurement = self.constructFullMeasurement(m, dd, amount)

    def constructFullMeasurement(self, m, dd, amount):
        """
        input:
            int:m       
            int:dd     
            int:amount 
        Constructs array [np.ndarray(amount,m,dd)] with each entry
        independently drawn from normal distribution, with
        0 being the expectation value and
        sqrt(1/(m*dd)) the standard deviation.
        curlA = [[A1],[A2],...,[Aamount]]
        with the norm of rows of all As set to one
        and every A hermitian.
        return np.ndarray of shape (amount,m,dd)
        """
        mu = 0
        sigma = (1.0 / (m * dd))**0.5
        # Draw all real entries from gaussian distribution
        normalMatrixReal = np.random.normal(mu, sigma, (m, dd * amount))
        # Make entries imaginary by multiplying with imaginary j
        normalMatrixImag = 1j * np.random.normal(mu, sigma, (m, dd * amount))
        # Gaussian matrix with real + imaginary entries
        normalMatrix = normalMatrixReal + normalMatrixImag
        # Reshape that Matrices are explicit As
        # Needed to make them hermitian
        littleMatrices = normalMatrix.reshape(
            amount, m, int(np.sqrt(dd)), int(np.sqrt(dd)))
        # Make As hermitian by (A + A^dag)/2
        normalMatrixHerm = (
            littleMatrices + np.transpose(littleMatrices,
                                          (0, 1, 3, 2)).conj()) / 2
        # Reshape again that multiplying with csSignal vector is easy
        normalMatrix = normalMatrixHerm.reshape(amount, m, dd)
        # Normalize the rows of big matrix 
        # First calculate the l2 norm of every row, stored in row vector
        normArray = np.sum(np.abs(normalMatrix)**2, axis=1)**(0.5)
        # Reshape to column vector
        normArray = normArray.reshape(amount, 1, dd)
        # Divide every row of normalMatrix by the l2 norm of itself
        normalMatrixNormed = normalMatrix / normArray
        return normalMatrixNormed

    def multiMatrixFunction(self, dagg=False):
        """
        return [array(amount,m,dd)]
        self.fullMeasurement is the full measurement matrix:
        """
        if not dagg:
            return self.fullMeasurement
        else:
            # transposes the single matrices inside the big matrix
            finalArray = np.transpose(self.fullMeasurement, [0, 2, 1])
            finalArray = finalArray.conj()
            return finalArray