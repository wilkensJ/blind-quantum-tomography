from Measurement import *
from SDTandALS import *
from csSignal import *
from plotFunctions import *
from ProcessData import *

# Initiate the dictionary for parameters regarding the
# signal and the measurement
sigMeasDict = {}
sigMeasDict["N"], sigMeasDict["dim"] = 7, 3**2
sigMeasDict["rank"], sigMeasDict["sparsity"] = 1, 2
sigMeasDict["listM"] = [3**4]


# Possible values for whichMType
sigMeasDict["whichMType"] = "CalibrationWithoutIdentity"
sigMeasDict["calibCoeffMultiply"] = .05
sigMeasDict["calibCoeffOffset"] = .2

# Initiate the dictionary for parameters regarding the
# algorithm
algParDict = {}
algParDict["it"] = 1000
algParDict["thresh"] = 1e-5
algParDict["runs"] = 50
algParDict["repetitions"] = 1
algParDict["listTomographyTypes"] = ["ALS"] # "SDT", "demixingT", "cheatDemixingT", "ALS"
algParDict["numberOfShots"] = False

# Initiate the dictionary for parameters regarding the
# boolean parameters
boolDict = {}
boolDict["initializationHack"] = False
boolDict["calibration"]        = True
boolDict["compareWStandardT"]  = True
boolDict["storeFiles"]         = True
boolDict["labBook"]            = False
boolDict["printVariables"]     = False
boolDict["randomTargetCoeff"]  = False
boolDict["firstTraceOne"]      = False
boolDict["allTraceOne"]        = False

# If required the ALS algorithm needs some extra parameters
ALSDict = {"alternatingIt":50, "initRandom":False, "numberOfTries":20}
# The function prearrangement runs everything,
# meaning that here the actual simulation is happening,
# and returns the directory in which the numerical data is stored
dirName = prearrangement(sigMeasDict, algParDict, boolDict, ALSDict)
# Write the relevant recovery parameters in a .txt file
# With the name str:storeFileName plus the tomography type ending
storeFileName = dirName + "coherentPauliFlipN10dim16r1s3"
storeRelevantParametersInTxtFile(dirName, storeFileName)
# Build the file name for SDT and standardT
nameSDT = storeFileName + "SDT.txt"
nameCWST = storeFileName + "compareWStandardT.txt"
# Plot the data
figureSize = (11, 4.75)
plt.figure(figsize = figureSize)
plotNumericalSimulationSDTandStandardTomography(
                                            nameSDT,
                                            nameCWST,
                                            sigMeasDict["calibCoeffOffset"])
# And save it
plt.savefig(storeFileName + ".png")
# And don't forget to show the plot
plt.show()
