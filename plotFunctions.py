# -*- coding: utf-8- -*-
"""

The functions in this python document plot the given data when giving
the name of the file as input.

The functions are used in the numerical study data plotting in

    Roth, Wilkens, Hangleiter and Eisert. 
    "Semi-device-dependent blind quantum tomography" 
    https://arxiv.org/abs/2006.03069

Authors: 
    Jadwiga Wilkens, Ingo Roth and Dominik Hangleiter


Licence: 
    This project is licensed under the MIT License -
    see the LICENSE.md file for details.
"""

import pdb
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick

# Define colors for whole document
infernofunc     = plt.get_cmap("inferno")
colorALS        = infernofunc(110)
colorSDT        = infernofunc(110)
colorST         = infernofunc(160)
colorCalibLevel = infernofunc(190)
colorDT         = infernofunc(200)
colorInformedDT = infernofunc(10)

def setFigureParameters(normal, smaller):
    """
    Input:
        int:normal,  normal font size
        int:smaller, smaller font size
    """
    # Define the different font sizes
    plt.rc('font', size=normal)           # controls default text sizes
    plt.rc('axes', titlesize=smaller)     # fontsize of the axes title
    plt.rc('axes', labelsize=normal)      # fontsize of the x and y labels
    plt.rc('xtick', labelsize=smaller)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=smaller)    # fontsize of the tick labels
    plt.rc('legend', fontsize=normal)     # legend fontsize
    plt.rc('figure', titlesize=normal)    # fontsize of the figure title

def storeRelevantParametersInTxtFileplotFunction(fileName,
                 shiftSign,
                 scatter = True,
                 color = (0.627847, 0.166575, 0.386276, 1.0),
                 whatToPlot = "traceNorms",
                 yScale = "log",
                 labelPlot = "data",
                 xLabel = "measurements m",
                 yLabel = "reconstruction error"):
    """
    This function plots the data which is contained in the str:fileName file.
    There is no plt.show() line!
    Input:
        str:fileName,     .txt file where data is stored in
        float:shiftSign,   amount the points are shifted in direction x axis sign
        str:whatToPlot,   Denotes the parameters which should be plotted:
                          "DeviationValues", "traceNorms", "neededIterations",
                          "objectiveFunctions", "calibrationCoefficients" 
        float:caliblevel, the level of calibration coefficients, if False
                          this is not needed
        str:yScale,       "log", "lin"
    """
    ### Parameters for the figure ###
    # Thickness of dashed lines
    lineWidth  = 2
    # Triangles position
    marker = 4
    # Marker size
    sSize  = 70
    # Extract all data
    allData = np.loadtxt(fileName)
    # Extract the assignements to amount of measurements for all data
    bigListM = allData[0]
    lengthM = int(len(bigListM)/5)
    # Get the x Axis
    listM = bigListM[:lengthM]
    # Calculate the shift between the two data sets
    differneceListM = listM[1:] - listM[:-1]
    minimalDifference = np.min(differneceListM)
    shiftAmount = minimalDifference/4.
    ### Extract all different types of data ###
    # ProbOfRec: for every amount of measurement point the probability
    # of recovery
    if whatToPlot == "DeviationValues":
        plotThis = allData[2:,:lengthM]
    # neededIterationsBig: for every point and every run the needed iterations
    if whatToPlot == "neededIterations":
        plotThis = allData[2:,lengthM:2*lengthM]
    # objectiveFunctionsBig: for every point and every run the objective 
    # function after the last iteration
    if whatToPlot == "objectiveFunctions":
        plotThis = allData[2:,2*lengthM:3*lengthM]
    # traceNormsBig: the distance of original and every recovered signal
    # in trace norm
    if whatToPlot == "traceNorms":
        plotThis = allData[2:,3*lengthM:4*lengthM]/2
    # calibrationcoefficientNormBig: the distance of original and every 
    # recovered calibration coefficients in l2 norm
    if whatToPlot == "calibrationCoefficients":
        plotThis = allData[2:,4*lengthM:5*lengthM]
    # Flatten the array to plot
    reshapedPlotThis = plotThis.reshape(len(plotThis)*len(plotThis[0]))
    # Calculate the median of every y point
    median = np.median(plotThis, axis = 0)
    # Stash up the listM's for plotting
    bigListMToPlot = np.tile(listM,len(plotThis[:,0]))
    ### Plot ###
    plt.yscale(yScale)
    if scatter:
        plt.scatter(bigListMToPlot - shiftSign * shiftAmount,
                    reshapedPlotThis,
                    marker = "_",
                    linewidth = lineWidth,
                    alpha = .5,
                    color = color)
    plt.scatter(listM - shiftSign * shiftAmount,
                median,
                marker = marker,
                s = sSize,
                color = color,
                label = labelPlot)
    plt.plot(listM- shiftSign * shiftAmount ,median,"--",color = color)
    ax = plt.gca()
    if yLabel:
        ax.set_ylabel(yLabel, fontdict=None, labelpad=-.4)
    if xLabel:
        ax.set_xlabel(xLabel, fontdict=None, labelpad=-.4)
    plt.tight_layout()

def plothLine(fileName, hlineLevel, plotLabel, color):
    """
    str:fileName
    float:hLineLevel
    str:plotLabel
    """
    allData = np.loadtxt(fileName)
    # Extract the assignments to amount of measurements for all data
    bigListM = allData[0]
    lengthM = int(len(bigListM)/5)
    # Get the x Axis
    listM = bigListM[:lengthM]
    plt.hlines(hlineLevel,
           listM[0],
           listM[-1],
           colors=color,
           linestyles='dashed',
           label=plotLabel)

def plotNumericalSimulationSDTandStandardTomography(fileNameSDT,
                                                    fileNameST,
                                                    calibrationLevel):
    """
    Only takes the two file Names as parameters and does the rest
    """
    ################################
    ######## Plot  ALS data ########
    ################################
    # Set the font sizes
    normal, smaller = 12, 11
    setFigureParameters(normal, smaller)
    # All colors are defined in the beginning
    # Define what variable to plot
    whatToPlot = "traceNorms"
    # More parameters for the plot
    yScale = "log"
    labelPlot = "data"
    xLabel = "measurements m"
    yLabel = "reconstruction error"
    # Plot it!
    plotFunction(fileNameSDT,
                 +1,
                 scatter = True,
                 color = colorALS,
                 whatToPlot = whatToPlot,
                 yScale = yScale,
                 labelPlot = "ALS",
                 xLabel = xLabel,
                 yLabel = yLabel)

    #################################
    # Plot Standard tomography data #
    #################################
    # Color defined up
    plotFunction(fileNameST,
                 -1,
                 scatter = True,
                 color = colorST,
                 whatToPlot = whatToPlot,
                 yScale = yScale,
                 labelPlot = "standard tomography",
                 xLabel = xLabel,
                 yLabel = yLabel)
    #######################################
    # Plot  calibration coefficient level #
    #######################################
    plothLine(fileNameST,
              calibrationLevel,
              "calibration coefficients level",
              colorCalibLevel)
    plt.legend(loc = 4)
    # Set the limits right!
    ax = plt.gca()
    ax.xaxis.set_major_formatter(mtick.ScalarFormatter(useMathText=True))

    # ax.set_xlim(left = 0, right = 250)
    ######################################
    # Plot inset calibration coefficient #
    ######################################
    # Make inset plot with calibration coefficients
    whatToPlot = "calibrationCoefficients"
    # Parameters for inset plot: [left, bottom, width, height]
    plt.axes([0.125, 0.21, 0.125, 0.249])
    # Make font smaller
    setFigureParameters(normal - 2, smaller - 1)
    # Plot!
    plotFunction(fileNameSDT,
                 +1,
                 scatter = False,
                 color = colorALS,
                 whatToPlot = whatToPlot,
                 yScale = yScale,
                 labelPlot = False,
                 xLabel = xLabel,
                 yLabel = yLabel)




def plotOnlyProbabilityOfRecovery(fileName, thresh, color, form, labelPlot):
    """
    Input:
        fileName,
        color,
        form,      "d", "s", "o"
        labelPlot,
        xLabel,
        yLabel
    """
    # Extract all data
    allData = np.loadtxt(fileName)
    # Extract the assignments to amount of measurements for all data
    bigListM = allData[0]
    lengthM = int(len(bigListM)/5)
    # Get the x Axis
    listM = bigListM[:lengthM]
    # Extract the deviation values of recovered signal from original signal
    deviationValues = allData[2:,:lengthM]
    # Count the amount of recovered signals for every amount of measurement m
    probOfRec = np.count_nonzero(deviationValues.T < thresh, axis=1)
    # Set the figure parameters
    normal, smaller = 12, 11
    setFigureParameters(normal, smaller)
    # Plot the single points
    plt.plot(listM, probOfRec, form,  markeredgewidth = 2, ms=7,
             color = color, fillstyle="none", label = labelPlot)
    # Plot the dashed line 
    plt.plot(listM, probOfRec,"--" ,color = color)
    # Make the legend
    plt.legend()
    # Set the x Axis and y Axis label
    ax = plt.gca()
    ax.set_ylabel("state recovery rate", fontdict=None, labelpad=1)
    ax.set_xlabel("measurements m", fontdict=None, labelpad=0)

def plotDTcheatingDTandSDTProbabilityOfRecovery(fileNameDT,
                                                fileNamecheatingDT,
                                                fileNameSDT):
    """
    Plots the three types of tomography algorithms
    """
    thresh = 1e-3
    plotOnlyProbabilityOfRecovery(fileNameDT,
                                  thresh,
                                  colorDT, 
                                  "d", 
                                  "DT")
    plotOnlyProbabilityOfRecovery(fileNamecheatingDT,
                                  thresh,
                                  colorInformedDT, 
                                  "s", 
                                  "informed DT")
    plotOnlyProbabilityOfRecovery(fileNameSDT,
                                  thresh,
                                  colorSDT, 
                                  "o", 
                                  "SDT")

